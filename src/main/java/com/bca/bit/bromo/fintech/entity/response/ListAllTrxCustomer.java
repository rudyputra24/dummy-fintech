package com.bca.bit.bromo.fintech.entity.response;

import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
public class ListAllTrxCustomer {
	
	private String trxDateTime;
	private List<GroupTrxCustomer> listTrxCust;
	
	@Data
	public static class GroupTrxCustomer{
		private UUID customerId;
		private List<TrxPerCustomer> listTrxPerCustomer;
		
		@Data
		public static class TrxPerCustomer{
			private UUID trxId;
			private UUID promoId;
			private String promoName;
			private String merchantId;
			private String merchantName;
		}
	}

}
