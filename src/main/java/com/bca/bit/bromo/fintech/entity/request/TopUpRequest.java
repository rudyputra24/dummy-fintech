package com.bca.bit.bromo.fintech.entity.request;

import lombok.Data;

@Data
public class TopUpRequest {
    private String fintech_id;
    private int amount;
}
