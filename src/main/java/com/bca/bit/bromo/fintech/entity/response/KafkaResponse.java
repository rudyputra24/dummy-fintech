package com.bca.bit.bromo.fintech.entity.response;

import lombok.Data;

@Data
public class KafkaResponse {
	
	private String topic;
	private String message;

}
