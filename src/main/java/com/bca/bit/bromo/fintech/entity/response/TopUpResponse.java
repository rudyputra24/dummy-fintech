package com.bca.bit.bromo.fintech.entity.response;

import lombok.Data;

@Data
public class TopUpResponse {
    private ErrorSchema error_schema;
    private OutputSchema output_schema;

    @Data
    public static class ErrorSchema {
        private String error_code;
        private ErrorMessage error_message;

        @Data
        public static class ErrorMessage {
            private String english;
            private String indonesian;
        }
    }

    @Data
    public static class OutputSchema {
        private String status;
    }
}
