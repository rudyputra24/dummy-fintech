package com.bca.bit.bromo.fintech.entity.response;

import lombok.Data;

import java.util.HashMap;

@Data
public class MessageMapEnglish {
    private static HashMap<String, String> messageMap;
    public static final String LANGUAGE_CODE = "EN";

    static {
        messageMap = new HashMap<String, String>();
        messageMap.put(ReturnCode.FINTECH_SERVICE_SUCCESS, "Success");
        messageMap.put(ReturnCode.FINTECH_DATA_NOT_FOUND, "Data not found");
        messageMap.put(ReturnCode.FINTECH_SERVICE_GENERAL_ERROR, "Error in processing service");
        messageMap.put(ReturnCode.FINTECH_CRED_NOT_ACTIVE, "Account not active");
        messageMap.put(ReturnCode.ACCESS_NOT_ALLOWED,"Access not allowed");
        messageMap.put(ReturnCode.WALLET_NOT_BINDED,"Wallet not binded");
        messageMap.put(ReturnCode.FINTECH_BALANCE_NOT_ENOUGH, "Balance not enough");
    }

    public static String getMessage(String messageCode){return messageMap.get(messageCode);}
}
