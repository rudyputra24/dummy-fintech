package com.bca.bit.bromo.fintech.entity.response;

import java.util.List;

import com.bca.bit.bromo.fintech.entity.Promo;
import com.bca.bit.bromo.fintech.entity.response.ListPromoResponse.ErrorSchema;
import com.bca.bit.bromo.fintech.entity.response.ListPromoResponse.OutputSchema;
import com.bca.bit.bromo.fintech.entity.response.ListPromoResponse.ErrorSchema.ErrorMessage;

import lombok.Data;

@Data
public class InquiryBalanceResponse {

	private ErrorSchema error_schema;
	private OutputSchema output_schema;

	@Data
	public static class ErrorSchema {
		private String error_code;
		private ErrorMessage error_message;

		@Data
		public static class ErrorMessage {
			private String english;
			private String indonesian;
		}
	}

	@Data
	public static class OutputSchema {
		private String balance;
		private String fintech_id;
		private String fintech_name;
		private String fintech_cust_name;
	}
}
