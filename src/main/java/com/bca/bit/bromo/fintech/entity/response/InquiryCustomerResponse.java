package com.bca.bit.bromo.fintech.entity.response;

import java.util.List;

import com.bca.bit.bromo.fintech.entity.response.InquiryStatementResponse.ErrorSchema;
import com.bca.bit.bromo.fintech.entity.response.InquiryStatementResponse.OutputSchema;
import com.bca.bit.bromo.fintech.entity.response.InquiryStatementResponse.ErrorSchema.ErrorMessage;
import com.bca.bit.bromo.fintech.entity.response.InquiryStatementResponse.OutputSchema.Statement;

import lombok.Data;

@Data
public class InquiryCustomerResponse {
	private ErrorSchema errorSchema;
	private OutputSchema outputSchema;

	@Data
	public static class ErrorSchema {
		private String errorCode;
		private ErrorMessage errorMessage;

		@Data
		public static class ErrorMessage {
			private String english;
			private String indonesian;
		}
	}

	@Data
	public static class OutputSchema {
		private String fintechId;
		private String fintechName;
		private String customerName;
		private String customerId;
	}
}
