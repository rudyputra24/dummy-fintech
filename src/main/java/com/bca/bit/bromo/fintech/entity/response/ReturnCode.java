package com.bca.bit.bromo.fintech.entity.response;

import lombok.Data;

@Data
public class ReturnCode {
    public static final String FINTECH_SERVICE_SUCCESS = "BIT-200";
    public static final String FINTECH_BALANCE_NOT_ENOUGH = "BIT-401";
    public static final String ACCESS_NOT_ALLOWED = "BIT-403";
    public static final String FINTECH_CRED_NOT_ACTIVE = "BIT-402";
    public static final String FINTECH_DATA_NOT_FOUND = "BIT-404";
    public static final String WALLET_NOT_BINDED = "BIT-300";
    public static final String FINTECH_SERVICE_GENERAL_ERROR = "BIT-499";
}
