package com.bca.bit.bromo.fintech.service;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import com.bca.bit.bromo.fintech.entity.response.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.bca.bit.bromo.fintech.entity.Customer;
import com.bca.bit.bromo.fintech.entity.Merchant;
import com.bca.bit.bromo.fintech.entity.Payment;
import com.bca.bit.bromo.fintech.entity.Promo;
import com.bca.bit.bromo.fintech.entity.response.InquiryStatementResponse.OutputSchema.Statement;
import com.bca.bit.bromo.fintech.entity.response.ListAllTrxCustomer.GroupTrxCustomer;
import com.bca.bit.bromo.fintech.entity.response.ListAllTrxCustomer.GroupTrxCustomer.TrxPerCustomer;
import com.bca.bit.bromo.fintech.entity.response.ListPromoResponse.OutputSchema;
import com.bca.bit.bromo.fintech.entity.response.ListPromoResponse.ErrorSchema;
import com.bca.bit.bromo.fintech.entity.response.ListPromoResponse.ErrorSchema.ErrorMessage;
import com.bca.bit.bromo.fintech.repo.CustomerRepo;
import com.bca.bit.bromo.fintech.repo.MerchantRepo;
import com.bca.bit.bromo.fintech.repo.PaymentRepo;
import com.bca.bit.bromo.fintech.repo.PromoRepo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class FintechService {

	@Autowired
	private CustomerRepo customerRepo;
	@Autowired
	private PromoRepo promoRepo;
	@Autowired
	private PaymentRepo paymentRepo;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private MerchantRepo merchantRepo;

	/*
	 * public ListPromoResponse getListPromo() { ListPromoResponse resp = new
	 * ListPromoResponse(); List<Promo> promoList = (List<Promo>)
	 * promoRepo.findAll(); OutputSchema outputSchema = new OutputSchema();
	 * ErrorMessage errMsg = new ErrorMessage(); ErrorSchema errSchema = new
	 * ErrorSchema();
	 * 
	 * if (promoList.size() > 0) { outputSchema.setPromo_list(promoList);
	 * errSchema.setError_code("BIT-200"); errMsg.setEnglish("Success");
	 * errMsg.setIndonesian("Sukses"); } else { errSchema.setError_code("BIT-404");
	 * errMsg.setEnglish("Data not found");
	 * errMsg.setIndonesian("Data tidak ditemukan"); }
	 * 
	 * errSchema.setError_message(errMsg); resp.setOutput_schema(outputSchema);
	 * resp.setError_schema(errSchema); return resp; }
	 */

	public InquiryBalanceResponse getBalance(String phoneNo, String fintechName) {
		InquiryBalanceResponse.OutputSchema outputSchema = new InquiryBalanceResponse.OutputSchema();
		InquiryBalanceResponse.ErrorSchema errSchema = new InquiryBalanceResponse.ErrorSchema();
		InquiryBalanceResponse.ErrorSchema.ErrorMessage errMsg = new InquiryBalanceResponse.ErrorSchema.ErrorMessage();

		InquiryBalanceResponse resp = new InquiryBalanceResponse();

		try {
			Customer cust = customerRepo.findByPhoneNoAndFintechName(phoneNo, fintechName);

			if (phoneNo.equals("") || phoneNo == null || cust == null) {
				errSchema.setError_code("BIT-404");
				errMsg.setEnglish("Data not found");
				errMsg.setIndonesian("Data tidak ditemukan");
			} else {
				outputSchema.setFintech_id(phoneNo);
				outputSchema.setBalance(Integer.toString(cust.getBalance()));
				outputSchema.setFintech_name(cust.getFintechName());
				outputSchema.setFintech_cust_name(cust.getFirstName() + " " + cust.getLastName());
				errSchema.setError_code("BIT-200");
				errMsg.setEnglish("Success");
				errMsg.setIndonesian("Sukses");
			}
		} catch (Exception e) {
			errSchema.setError_code("BIT-500");
			errMsg.setEnglish("Internal Server Error");
			errMsg.setIndonesian("Internal Server Error");
		}

		errSchema.setError_message(errMsg);
		resp.setOutput_schema(outputSchema);
		resp.setError_schema(errSchema);

		return resp;
	}

	public InquiryStatementResponse getStatement(String phoneNo, String fintechName) {
		InquiryStatementResponse resp = new InquiryStatementResponse();

		InquiryStatementResponse.OutputSchema outputSchema = new InquiryStatementResponse.OutputSchema();
		InquiryStatementResponse.ErrorSchema errSchema = new InquiryStatementResponse.ErrorSchema();
		InquiryStatementResponse.ErrorSchema.ErrorMessage errMsg = new InquiryStatementResponse.ErrorSchema.ErrorMessage();

		try {
			Customer cust = customerRepo.findByPhoneNoAndFintechName(phoneNo, fintechName);

			if (phoneNo.equals("") || phoneNo == null || cust == null) {

				errSchema.setError_code("BIT-404");
				errMsg.setEnglish("Data not found");
				errMsg.setIndonesian("Data tidak ditemukan");
			} else {
				List<Payment> listPayment = new ArrayList<Payment>();
				List<Statement> listStatement = new ArrayList<Statement>();
				Merchant merchant;
				listPayment = paymentRepo.findTop50ByCustomerIdOrderByTrxDateDesc(cust.getCustomerId());

				for (Payment paymentRow : listPayment) {
					Statement statement = new Statement();
					modelMapper.map(paymentRow, statement);
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmXXX");
					String dateISO = df.format(paymentRow.getTrxDate());
					statement.setTrxDate(dateISO);

					if (paymentRow.getPromoId() != null && paymentRow.getMerchantId() != null) {
						merchant = merchantRepo.findByMerchantId(paymentRow.getMerchantId());
						statement.setDesc("Payment in " + merchant.getMerchantName());
					} else {
						statement.setDesc("Top up");
					}
					listStatement.add(statement);
				}

				outputSchema.setPaymentList(listStatement);
				outputSchema.setFintechName(fintechName);
				errSchema.setError_code("BIT-200");
				errMsg.setEnglish("Success");
				errMsg.setIndonesian("Sukses");
			}
		} catch (Exception e) {
			errSchema.setError_code("BIT-500");
			errMsg.setEnglish("Internal Server Error");
			errMsg.setIndonesian("Internal Server Error");
			e.printStackTrace();
		}

		outputSchema.setFintechId(phoneNo);
		errSchema.setError_message(errMsg);
		resp.setOutput_schema(outputSchema);
		resp.setError_schema(errSchema);

		return resp;
	}

	public InquiryCustomerResponse getCustomer(String phoneNo, String fintechName) {
		InquiryCustomerResponse resp = new InquiryCustomerResponse();

		InquiryCustomerResponse.ErrorSchema errSchema = new InquiryCustomerResponse.ErrorSchema();
		InquiryCustomerResponse.ErrorSchema.ErrorMessage errMsg = new InquiryCustomerResponse.ErrorSchema.ErrorMessage();
		InquiryCustomerResponse.OutputSchema outputSchema = new InquiryCustomerResponse.OutputSchema();

		try {
			Customer cust = customerRepo.findByPhoneNoAndFintechName(phoneNo, fintechName);

			if (phoneNo.equals("") || phoneNo == null || cust == null) {
				errSchema.setErrorCode("BIT-404");
				errMsg.setEnglish("Data not found");
				errMsg.setIndonesian("Data tidak ditemukan");
			} else {
				outputSchema.setFintechId(phoneNo);
				outputSchema.setCustomerName(cust.getFirstName() + " " + cust.getLastName());
				outputSchema.setFintechName(cust.getFintechName());
				outputSchema.setCustomerId(cust.getCustomerId().toString());
				errSchema.setErrorCode("BIT-200");
				errMsg.setEnglish("Success");
				errMsg.setIndonesian("Sukses");
			}

		} catch (Exception e) {
			errSchema.setErrorCode("BIT-500");
			errMsg.setEnglish("Internal Server Error");
			errMsg.setIndonesian("Internal Server Error");
		}

		outputSchema.setFintechId(phoneNo);
		errSchema.setErrorMessage(errMsg);
		resp.setOutputSchema(outputSchema);
		resp.setErrorSchema(errSchema);

		return resp;
	}

	public VerifyUnbindResponse verifyForUnbind(String fintechId, String fintechName, String appCode,
			String bindAppCustId) {

		try {
			if (appCode.equals("bromo_app")) {
				Customer cust = customerRepo.findByPhoneNoAndFintechName(fintechId, fintechName);

				if (cust != null) {
					if (cust.getStatus().equals("ACT")) {
						if (cust.getBromoId() != null) {
							if (bindAppCustId == null || bindAppCustId.equals("")) {
								return new VerifyUnbindResponse(ReturnCode.ACCESS_NOT_ALLOWED, "failed", "active");
							}else if (cust.getBromoId().equals(UUID.fromString(bindAppCustId))) {
								cust.setBromoId(null);
								customerRepo.save(cust);
								return new VerifyUnbindResponse(ReturnCode.FINTECH_SERVICE_SUCCESS, "success",
										"active");
							} else {
								return new VerifyUnbindResponse(ReturnCode.ACCESS_NOT_ALLOWED, "failed", "active");
							}
						} else {
							return new VerifyUnbindResponse(ReturnCode.WALLET_NOT_BINDED, "failed", "active");
						}
					} else {
						return new VerifyUnbindResponse(ReturnCode.FINTECH_SERVICE_GENERAL_ERROR, "failed",
								"not active");
					}
				} else {
					return new VerifyUnbindResponse(ReturnCode.FINTECH_DATA_NOT_FOUND);
				}
			} else {
				return new VerifyUnbindResponse(ReturnCode.ACCESS_NOT_ALLOWED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new VerifyUnbindResponse(ReturnCode.FINTECH_SERVICE_GENERAL_ERROR);
		}

	}

	public VerifyBindResponse verifyForBind(String fintech_id, String fintech_name, String app_code,
			String binded_app_id) {

		VerifyBindResponse verifyBindResp = new VerifyBindResponse();
		VerifyBindResponse.ErrorSchema.ErrorMessage errMsg = new VerifyBindResponse.ErrorSchema.ErrorMessage();
		VerifyBindResponse.ErrorSchema errSchema = new VerifyBindResponse.ErrorSchema();
		VerifyBindResponse.OutputSchema outputSchema = new VerifyBindResponse.OutputSchema();

		try {

			if (app_code.equals("bromo_app")) {

				Customer cust = customerRepo.findByPhoneNoAndFintechName(fintech_id, fintech_name);

				if (cust == null) {
					errMsg.setEnglish("Customer not found");
					errMsg.setIndonesian("Customer tidak ditemukan");
					errSchema.setErrorCode("BIT-404");
					errSchema.setErrorMessage(errMsg);
					outputSchema.setSuccessFlag("failed");
					outputSchema.setStatusCustomer(null);
				} else {

					if (cust.getStatus().equals("ACT")) {
						if (cust.getBromoId() == null || cust.getBromoId().equals("")) {

							errMsg.setEnglish("Success");
							errMsg.setIndonesian("Sukses");
							errSchema.setErrorCode("BIT-200");
							errSchema.setErrorMessage(errMsg);
							outputSchema.setSuccessFlag("success");
							outputSchema.setStatusCustomer("active");

							cust.setBromoId(UUID.fromString(binded_app_id));
							customerRepo.save(cust);

						} else {
							errMsg.setEnglish("Failed");
							errMsg.setIndonesian("Gagal");
							errSchema.setErrorCode("BIT-500");
							errSchema.setErrorMessage(errMsg);
							outputSchema.setSuccessFlag("binded");
							outputSchema.setStatusCustomer("active");
						}

					} else {
						errMsg.setEnglish("Failed");
						errMsg.setIndonesian("Gagal");
						errSchema.setErrorCode("BIT-500");
						errSchema.setErrorMessage(errMsg);
						outputSchema.setSuccessFlag("failed");
						outputSchema.setStatusCustomer("not active");
					}
				}

			} else {

				errMsg.setEnglish("App not allowed");
				errMsg.setIndonesian("Aplikasi tidak diizinkan");
				errSchema.setErrorCode("BIT-500");
				errSchema.setErrorMessage(errMsg);
				outputSchema.setSuccessFlag("failed");
				outputSchema.setStatusCustomer(null);
			}

		} catch (Exception e) {

			System.out.println("masuk exception bung");

			errMsg.setEnglish("Internal Server Error");
			errMsg.setIndonesian("Internal Server Error");
			errSchema.setErrorCode("BIT-500");
			errSchema.setErrorMessage(errMsg);
			outputSchema.setSuccessFlag("failed");
			outputSchema.setStatusCustomer("failed");

			e.printStackTrace();
		}

		verifyBindResp.setErrorSchema(errSchema);
		verifyBindResp.setOutputSchema(outputSchema);

		return verifyBindResp;
	}

	public TopUpResponse doTopUp(String fintech_id, int amount) {
		TopUpResponse topUpResponse = new TopUpResponse();
		TopUpResponse.ErrorSchema errorSchema = new TopUpResponse.ErrorSchema();
		TopUpResponse.OutputSchema outputSchema = new TopUpResponse.OutputSchema();
		TopUpResponse.ErrorSchema.ErrorMessage errorMessage = new TopUpResponse.ErrorSchema.ErrorMessage();
		Customer customer = new Customer();
		customer = customerRepo.findByPhoneNo(fintech_id);
		if (customer.getCustomerId().toString().isEmpty()) {
			errorMessage.setIndonesian("Salah Nomor");
			errorMessage.setEnglish("Wrong fintech Id");
			errorSchema.setError_code("BIT-400");
			errorSchema.setError_message(errorMessage);
			topUpResponse.setError_schema(errorSchema);
		} else {
			int balance = customer.getBalance();
			int newBalance = balance + amount;
			customer.setBalance(newBalance);
			customerRepo.save(customer);
			errorMessage.setEnglish("Success");
			errorMessage.setIndonesian("Berhasil");
			errorSchema.setError_message(errorMessage);
			errorSchema.setError_code("BIT-200");
			outputSchema.setStatus("success");
			topUpResponse.setError_schema(errorSchema);
			topUpResponse.setOutput_schema(outputSchema);
		}

		return topUpResponse;
	}

	public String listTrxCustomerKafka() {

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();

		ListAllTrxCustomer listAllTrxCustomer = new ListAllTrxCustomer();
		List<GroupTrxCustomer> listGroupTrxCustomer = new ArrayList<GroupTrxCustomer>();
		List<TrxPerCustomer> listTrxPerCustomer = new ArrayList<ListAllTrxCustomer.GroupTrxCustomer.TrxPerCustomer>();
		Iterable<Customer> custList = customerRepo.findAll();
		// for()

		for (Customer c : custList) {
			listTrxPerCustomer.clear();
			GroupTrxCustomer groupTrxCustomer = new GroupTrxCustomer();
			groupTrxCustomer.setCustomerId(c.getCustomerId());
			listGroupTrxCustomer.add(groupTrxCustomer);

			Calendar calendarNow = Calendar.getInstance();
			calendarNow.setTime(new Date(System.currentTimeMillis()));

			Calendar calendarOneHourAgo = Calendar.getInstance();
			calendarOneHourAgo.setTime(new Date(System.currentTimeMillis()));
			calendarOneHourAgo.add(Calendar.HOUR, -1);

			System.out.println("Now : " + calendarNow.getTime());
			System.out.println("Yesterday : " + calendarOneHourAgo.getTime());

			for (Payment p : c.getTrx()) {
				if (p.getTrxDate().before(calendarNow.getTime())
						&& p.getTrxDate().after(calendarOneHourAgo.getTime())) {
					System.out.println(p.toString());
					if (p.getMerchantId() != null) {
						TrxPerCustomer trxPerCust = new TrxPerCustomer();
						trxPerCust.setMerchantId(p.getMerchantId().toString());
						trxPerCust.setMerchantName(merchantRepo.findByMerchantId(p.getMerchantId()).getMerchantName());
						trxPerCust.setPromoId(p.getPromoId());
						trxPerCust.setPromoName(promoRepo.findByPromoId(p.getPromoId()).getPromoHeader());
						trxPerCust.setTrxId(p.getTrxId());
						listTrxPerCustomer.add(trxPerCust);
					}
				}
			}

			groupTrxCustomer.setListTrxPerCustomer(listTrxPerCustomer);
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		String dateNow = simpleDateFormat.format(new Date(System.currentTimeMillis()));
		listAllTrxCustomer.setTrxDateTime(dateNow);
		listAllTrxCustomer.setListTrxCust(listGroupTrxCustomer);
		String result = gson.toJson(listAllTrxCustomer);

		return result;

	}

	public String listTrxCustomerKafkaDummy() {

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();

		ListAllTrxCustomer listAllTrxCustomer = new ListAllTrxCustomer();
		List<GroupTrxCustomer> listGroupTrxCustomer = new ArrayList<GroupTrxCustomer>();
		List<TrxPerCustomer> listTrxPerCustomer = new ArrayList<ListAllTrxCustomer.GroupTrxCustomer.TrxPerCustomer>();
		Iterable<Customer> custList = customerRepo.findAll();
		// for()

		for (Customer c : custList) {
			listTrxPerCustomer.clear();
			GroupTrxCustomer groupTrxCustomer = new GroupTrxCustomer();
			groupTrxCustomer.setCustomerId(c.getCustomerId());
			listGroupTrxCustomer.add(groupTrxCustomer);

			List<Payment> listPayment = paymentRepo.findByCustomerId(c.getCustomerId());

			for (Payment p : listPayment) {

				if (p.getMerchantId() != null) {
					System.out.println(p.toString());
					TrxPerCustomer trxPerCust = new TrxPerCustomer();
					trxPerCust.setMerchantId(p.getMerchantId().toString());
					trxPerCust.setMerchantName(merchantRepo.findByMerchantId(p.getMerchantId()).getMerchantName());
					trxPerCust.setPromoId(p.getPromoId());
					trxPerCust.setPromoName(promoRepo.findByPromoId(p.getPromoId()).getPromoHeader());
					trxPerCust.setTrxId(p.getTrxId());
					listTrxPerCustomer.add(trxPerCust);
				}
			}

			groupTrxCustomer.setListTrxPerCustomer(listTrxPerCustomer);
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		String dateNow = simpleDateFormat.format(new Date(System.currentTimeMillis()));
		listAllTrxCustomer.setTrxDateTime(dateNow);
		listAllTrxCustomer.setListTrxCust(listGroupTrxCustomer);
		String result = gson.toJson(listAllTrxCustomer);

		return result;

	}

}
