package com.bca.bit.bromo.fintech.entity.response;

import lombok.Data;

import java.util.HashMap;

@Data
public class MessageMapIndonesian {
    private static HashMap<String, String> messageMap;
    public static final String LANGUAGE_CODE = "ID";

    static {
        messageMap = new HashMap<String, String>();
        messageMap.put(ReturnCode.FINTECH_SERVICE_SUCCESS, "Berhasil");
        messageMap.put(ReturnCode.FINTECH_DATA_NOT_FOUND, "Data tidak ditemukan");
        messageMap.put(ReturnCode.FINTECH_SERVICE_GENERAL_ERROR, "Terjadi kesalahan dalam proses");
        messageMap.put(ReturnCode.ACCESS_NOT_ALLOWED,"Akses tidak diizinkan");
        messageMap.put(ReturnCode.FINTECH_CRED_NOT_ACTIVE, "Akun tidak aktif");
        messageMap.put(ReturnCode.WALLET_NOT_BINDED,"Wallet tidak sedang dihubungkan");
        messageMap.put(ReturnCode.FINTECH_BALANCE_NOT_ENOUGH, "Saldo tidak cukup");
    }

    public static String getMessage(String messageCode){return messageMap.get(messageCode);}
}
