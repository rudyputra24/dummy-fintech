package com.bca.bit.bromo.fintech.entity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="ms_merchant")
public class Merchant {
	
	@Id
	@Column(name="merchant_id")
	private UUID merchantId;
	
	@Column(name="merchant_name")
	private String merchantName;
	
	@Column(name="merchant_address")
	private String merchantAddress;
	
	@Column(name="merchant_loc_city")
	private String merchantLocCity;
	
	@Column(name="merchant_state")
	private String merchantState;
	
	@Column(name="merchant_phone")
	private String merchantPhone;
	
	@Column(name="merchant_contact_person")
	private String merchantContactPerson;
	
	@Column(name="merchant_category")
	private String merchantCategory;
	
	@Column(name="merchant_latitude")
	private String merchantLatitude;
	
	@Column(name="merchant_longitude")
	private String merchantLongitude;
	
	@Column(name="fintech_name")
	private String fintechName;
	
	
}
