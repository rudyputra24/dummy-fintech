package com.bca.bit.bromo.fintech.repo;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.bca.bit.bromo.fintech.entity.Customer;

@Repository
public interface CustomerRepo extends PagingAndSortingRepository<Customer, UUID> {
	
	Customer findByPhoneNoAndFintechName(String phoneNo,String fintechName);

	Customer findByPhoneNo(String phoneNo);
}
