package com.bca.bit.bromo.fintech.entity.response;


import lombok.Data;

@Data
public class BaseResponse {

	private ErrorSchema errorSchema;
	
    public BaseResponse() {
        super();
    }

    public BaseResponse(String returnCode) {
        String english = MessageMapEnglish.getMessage(returnCode);
        String indonesian = MessageMapIndonesian.getMessage(returnCode);
        errorSchema = new ErrorSchema(returnCode,indonesian,english);
    }

    @Data
    public static class ErrorSchema{
        public ErrorSchema(String error_code, String indonesian, String english){
            this.errorCode = error_code;
            errorMessage = new ErrorMessage(indonesian,english);
        }

        private String errorCode;
        private ErrorMessage errorMessage;

        @Data
        public static class ErrorMessage{
            public ErrorMessage(String indonesian, String english){
                this.english = english;
                this.indonesian = indonesian;
            }
            private String english;
            private String indonesian;
        }
    }
}
