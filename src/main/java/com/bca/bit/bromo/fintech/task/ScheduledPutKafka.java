package com.bca.bit.bromo.fintech.task;

import java.sql.Date;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.bca.bit.bromo.fintech.service.FintechService;

@Component
public class ScheduledPutKafka {

	@Autowired
	private FintechService fintechSvc;
	
	@Value("${kafka.url}")
	private String url;

	//@Scheduled(cron = "* * * ? * *")
	public void putTransactionIntoKafka() {
		String kafkaDummy = fintechSvc.listTrxCustomerKafkaDummy();
		RestTemplate restTemplate = new RestTemplate();
		
		String fintechCompany = "OVO";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		String dateNow = simpleDateFormat.format(new Date(System.currentTimeMillis()));
		String topic = fintechCompany+"-"+dateNow;

		System.out.println("Url : "+url);
		// String URL = "http://10.20.214.63:8003";
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url).path("/mobile").path("/put")
				.queryParam("topic", topic);

		try {
			HttpEntity<String> kafkaDummyBody = new HttpEntity<String>(kafkaDummy);
			ResponseEntity<String> putKafkaResponse = restTemplate.exchange(builder.toUriString(), HttpMethod.POST,
					kafkaDummyBody, String.class);

			if (putKafkaResponse.getStatusCode().equals(HttpStatus.OK)) {
				System.out.println("KAFKA PUT MESSAGE BERHASIL");
			} else {
				throw new Exception("KAFKA PUT MESSAGE ERROR");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
