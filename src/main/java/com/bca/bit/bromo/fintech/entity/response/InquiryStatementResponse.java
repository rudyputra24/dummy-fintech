package com.bca.bit.bromo.fintech.entity.response;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import com.bca.bit.bromo.fintech.entity.Payment;
import com.bca.bit.bromo.fintech.entity.Promo;
import com.bca.bit.bromo.fintech.entity.response.ListPromoResponse.ErrorSchema;
import com.bca.bit.bromo.fintech.entity.response.ListPromoResponse.OutputSchema;
import com.bca.bit.bromo.fintech.entity.response.ListPromoResponse.ErrorSchema.ErrorMessage;

import lombok.Data;

@Data
public class InquiryStatementResponse {
	private ErrorSchema error_schema;
	private OutputSchema output_schema;

	@Data
	public static class ErrorSchema {
		private String error_code;
		private ErrorMessage error_message;

		@Data
		public static class ErrorMessage {
			private String english;
			private String indonesian;
		}
	}

	@Data
	public static class OutputSchema {
		private String fintechId;
		private String fintechName;
		private List<Statement> paymentList;
		
		@Data
		public static class Statement{
			private String trxId;
			private String trxDate;
			private String desc;
			private String cashback;
			private String totalPayment;
			private String topUp;
		}
	}
}
