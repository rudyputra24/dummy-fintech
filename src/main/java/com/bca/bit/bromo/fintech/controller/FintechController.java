package com.bca.bit.bromo.fintech.controller;

import com.bca.bit.bromo.fintech.entity.request.TopUpRequest;
import com.bca.bit.bromo.fintech.entity.response.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.bca.bit.bromo.fintech.entity.request.RequestBindBromo;
import com.bca.bit.bromo.fintech.service.FintechService;
import com.bca.bit.bromo.fintech.task.ScheduledPutKafka;
import com.google.gson.Gson;

@RestController
@RequestMapping("/fintech/inquiry")
public class FintechController {
	
	@Autowired
	private FintechService fintechSvc;
	
	/*
	@GetMapping("/promo")
	public ListPromoResponse getListPromo() {
		ListPromoResponse resp = new ListPromoResponse();
		resp = fintechSvc.getListPromo();
		return resp;
	}*/
	
	/*
	@GetMapping("/transaction")
	public String getAllTransactionInDay() {
		//ListAllTrxCustomer listAllTrxCustomer = scheduledTask.listTrxCustomerKafka();
		//System.out.println(listAllTrxCustomer.getTrxDate());
		//return listAllTrxCustomer;
		return scheduledTask.listTrxCustomerKafkaDummy();
	}*/
	
	@PutMapping("/verify/bind")
	public VerifyBindResponse verifyForBind(@RequestBody RequestBindBromo request) {
		VerifyBindResponse resp = new VerifyBindResponse();
		resp = fintechSvc.verifyForBind(request.getFintechId(),request.getFintechName(),request.getAppCode(),request.getBindAppCustId());
		return resp;
	}
	
	@PutMapping("/verify/unbind")
	public VerifyUnbindResponse verifyForUnbind(@RequestBody RequestBindBromo request) {
		VerifyUnbindResponse resp = fintechSvc.verifyForUnbind(request.getFintechId(),request.getFintechName(),request.getAppCode(),request.getBindAppCustId());
		return resp;
	}
	
	@GetMapping("/balance")
	public InquiryBalanceResponse getBalance(@RequestParam String fintech_id,@RequestParam String fintech_name) {
		InquiryBalanceResponse resp = new InquiryBalanceResponse();
		resp = fintechSvc.getBalance(fintech_id,fintech_name);
		return resp;
	}
	
	@GetMapping("/statement")
	public InquiryStatementResponse getStatement(@RequestParam String fintech_id,@RequestParam String fintech_name) {
		InquiryStatementResponse resp = new InquiryStatementResponse();
		resp = fintechSvc.getStatement(fintech_id,fintech_name);
		return resp;
	}
	
	@GetMapping("/customer")
	public InquiryCustomerResponse getCustomer(@RequestParam String fintech_id,@RequestParam String fintech_name) {
		InquiryCustomerResponse resp = new InquiryCustomerResponse();
		resp = fintechSvc.getCustomer(fintech_id,fintech_name);
		return resp;
	}

	@PutMapping("/topup")
    public TopUpResponse doTopUp (@RequestBody TopUpRequest topUpRequest){
	    TopUpResponse topUpResponse = new TopUpResponse();
	    topUpResponse = fintechSvc.doTopUp(topUpRequest.getFintech_id(), topUpRequest.getAmount());
	    return topUpResponse;
    }

}
