package com.bca.bit.bromo.fintech.repo;

import java.util.UUID;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.bca.bit.bromo.fintech.entity.Merchant;

@Repository
public interface MerchantRepo extends PagingAndSortingRepository<Merchant, UUID> {

	Merchant findByMerchantId(UUID id);
	
}
