package com.bca.bit.bromo.fintech.entity.response;

import java.util.Date;
import java.util.List;

import com.bca.bit.bromo.fintech.entity.Promo;

import lombok.Data;

@Data
public class ListPromoResponse {
	private ErrorSchema error_schema;
	private OutputSchema output_schema;

	@Data
	public static class ErrorSchema {
		private String error_code;
		private ErrorMessage error_message;

		@Data
		public static class ErrorMessage {
			private String english;
			private String indonesian;
		}
	}

	@Data
	public static class OutputSchema {
		
		private List<Promo> promo_list;
		
		/*
		@Data
		public static class PromoList{
			private String promo_id;
			private String promo_header;
			private Date start_date;
			private Date end_date;
			private String description;
			private String merchant_name;
			private String merchant_address;
			private String merchant_coordinate;
		}*/
		
	}

}
