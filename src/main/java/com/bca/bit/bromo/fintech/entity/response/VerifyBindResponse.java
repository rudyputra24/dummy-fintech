package com.bca.bit.bromo.fintech.entity.response;

import java.util.List;

import com.bca.bit.bromo.fintech.entity.Merchant;
import com.bca.bit.bromo.fintech.entity.Payment;

import lombok.Data;

@Data
public class VerifyBindResponse {

	private ErrorSchema errorSchema;
	private OutputSchema outputSchema;

	@Data
	public static class ErrorSchema {
		private String errorCode;
		private ErrorMessage errorMessage;

		@Data
		public static class ErrorMessage {
			private String english;
			private String indonesian;
		}
	}

	@Data
	public static class OutputSchema {
		private String successFlag;
		private String statusCustomer;
	}
}
