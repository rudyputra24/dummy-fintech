package com.bca.bit.bromo.fintech.entity;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="ms_promo")
public class Promo {

	@Id
	@Column(name="promo_id")
	private UUID promoId;
	@Column(name="promo_header")
	private String promoHeader;
	@Column(name="start_date")
	private Date startDate;
	@Column(name="end_date")
	private Date endDate;
	@Column(name="description")
	private String description;
	@Column(name="fintech_name")
	private String fintechName;
	@OneToMany
	@JoinColumn(name="promo_id")
	private List<Merchant> merchants; 
	
}
