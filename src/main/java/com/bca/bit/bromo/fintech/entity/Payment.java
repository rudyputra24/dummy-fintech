package com.bca.bit.bromo.fintech.entity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name="trx_payment")
public class Payment {
	
	@Id
	@Column(name="trx_id")
	private UUID trxId;
	
	@Column(name="trx_date")
	private Date trxDate;
	
	@Column(name="promo_id")
	private UUID promoId;
	
	@Column(name="customer_id")
	private UUID customerId;
	
	@Column(name="merchant_id")
	private UUID merchantId;
	
	@Column(name="cashback")
	private String cashback;
	
	@Column(name="top_up")
	private String topUp;
	
	@Column(name="total_payment")
	private String totalPayment;
	
	@Column(name="fintech_name")
	private String fintechName;
	
}
