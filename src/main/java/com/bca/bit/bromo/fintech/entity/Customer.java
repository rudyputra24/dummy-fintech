package com.bca.bit.bromo.fintech.entity;

import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="ms_customer")
public class Customer {

	@Id
	@Column(name="customer_id")
	private UUID customerId;
	@Column(name="phone_no")
	private String phoneNo;
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	@Column(name="gender")
	private String gender;
	@Column(name="balance")
	private int balance;
	@Column(name="fintech_name")
	private String fintechName;
	@Column(name="status")
	private String status;
	@Column(name="bromo_id")
	private UUID bromoId;
	@OneToMany
	@JoinColumn(name="customer_id")
	private List<Payment> trx;
}
