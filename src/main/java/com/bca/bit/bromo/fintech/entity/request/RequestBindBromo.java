package com.bca.bit.bromo.fintech.entity.request;

import lombok.Data;

@Data
public class RequestBindBromo {

	private String fintechId;
	private String fintechName;
	private String appCode;
	private String bindAppCustId;
	
}
