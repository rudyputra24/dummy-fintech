package com.bca.bit.bromo.fintech.repo;

import java.util.UUID;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.bca.bit.bromo.fintech.entity.Promo;

@Repository
public interface PromoRepo extends PagingAndSortingRepository<Promo, UUID> {

	Promo findByPromoId(UUID id);
}
