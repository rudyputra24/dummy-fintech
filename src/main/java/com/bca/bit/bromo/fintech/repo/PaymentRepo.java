package com.bca.bit.bromo.fintech.repo;

import java.util.List;
import java.util.UUID;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.bca.bit.bromo.fintech.entity.Payment;

@Repository
public interface PaymentRepo extends PagingAndSortingRepository<Payment, UUID> {

	List<Payment> findTop50ByCustomerIdOrderByTrxDateDesc(UUID custId);
	List<Payment> findByCustomerId(UUID custId);
}
