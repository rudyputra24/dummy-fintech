package com.bca.bit.bromo.fintech.entity.response;

import lombok.Data;

@Data
public class VerifyUnbindResponse extends BaseResponse{
	
	private VerifyUnbindResponseOutputSchema outputSchema;

	public VerifyUnbindResponse(String error_code) {
		super(error_code);
	}
	
	public VerifyUnbindResponse(String error_code,String success_flag,String status_customer) {
		super(error_code);
		this.outputSchema = new VerifyUnbindResponseOutputSchema(success_flag, status_customer);
	}
	
	@Data
	private class VerifyUnbindResponseOutputSchema {
		private String successFlag;
		private String statusCustomer;
		
		public VerifyUnbindResponseOutputSchema(String successFlag,String statusCustomer) {
			this.successFlag = successFlag;
			this.statusCustomer = statusCustomer;
		}
	}
}
